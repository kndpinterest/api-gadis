from django.db import models
from .product import Product, Authenticate, timezone


class Category(models.Model):
    public_id = models.CharField(max_length=225, null=False, unique=True)
    author = models.ForeignKey(
        Authenticate, on_delete=models.CASCADE, related_name='author_category_product')
    name = models.CharField(max_length=225, null=False, unique=True)
    product = models.ManyToManyField(Product, related_name='product_many')
    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now_add=True)

    def save(self, *args, **kwargs):
        self.update_at = timezone.now()
        super().save(*args, **kwargs)
