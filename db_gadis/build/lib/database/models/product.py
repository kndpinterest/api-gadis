from django.db import models
from location_field.models.plain import PlainLocationField
from .authenticate import Authenticate
from django.utils import timezone
from .comment import Comment
from django.utils.translation import gettext as _


class SupportProduct(models.Model):
    author = models.ForeignKey(
        Authenticate, on_delete=models.CASCADE, related_name='author_relationship')
    info = models.CharField(max_length=225, null=False, default=_(
        "Someone provides you with support for your product"))
    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField()

    def save(self, *args, **kwargs):
        self.update_at = timezone.now()
        super().save(*args, **kwargs)

    class Meta:
        ordering = ['-create_at']


class ProductImage(models.Model):
    image = models.ImageField(upload_to='product/')
    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField()

    def save(self, *args, **kwargs):
        self.update_at = timezone.now()
        super().save(*args, **kwargs)

    class Meta:
        ordering = ['-create_at']


class SoftImageForRemove(models.Model):
    public_id = models.CharField(max_length=225, null=False, unique=True)
    image = models.ManyToManyField(ProductImage, related_name='+')
    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField()
    already_product = models.OneToOneField(
        'Product', on_delete=models.CASCADE, unique=True, null=True, related_name='already_product_relationhip')

    def save(self, *args, **kwargs):
        self.update_at = timezone.now()
        super().save(*args, **kwargs)


class Location(models.Model):
    country = models.CharField(max_length=225, null=False)
    address = models.TextField(null=True)
    city = models.CharField(max_length=225, null=False)
    location = PlainLocationField(based_fields=['city'], zoom=7, null=True)


class Product(models.Model):
    public_id = models.CharField(max_length=225, null=False, unique=True)
    author = models.ForeignKey(Authenticate, on_delete=models.CASCADE)

    name = models.CharField(max_length=225, null=False)
    description = models.TextField(null=False)
    sold_out = models.BooleanField(default=False)
    price = models.DecimalField(max_digits=19, decimal_places=2)
    image = models.ForeignKey(SoftImageForRemove, on_delete=models.CASCADE,
                              null=True, related_name='image_relationship')

    location = models.OneToOneField(
        Location, on_delete=models.CASCADE, related_name='location_one_field', null=True)

    total = models.IntegerField(default=0)
    sold_total = models.IntegerField(default=0)

    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField()
    comment = models.ManyToManyField(
        Comment, related_name='comment_many_to_many')
    support = models.ManyToManyField(
        SupportProduct, related_name='support_many_to_many')

    class Meta:
        ordering = ['-create_at']

    def save(self, *args, **kwargs):
        self.update_at = timezone.now()
        super().save(*args, **kwargs)
