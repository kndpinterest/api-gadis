from database.models.constant.types import EnumGenderChoice
from django.db import models
from django.utils import timezone
from .base import Base, CustomUser
from phonenumber_field.modelfields import PhoneNumberField


class Interest(models.Model):
    name = models.CharField(max_length=225, null=False)
    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField()

    def save(self, *args, **kwargs):
        self.update_at = timezone.now()
        super().save(*args, **kwargs)


class Available(models.Model):
    name = models.CharField(max_length=225, null=False)
    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField()

    def save(self, *args, **kwargs):
        self.update_at = timezone.now()
        super().save(*args, **kwargs)


class Authenticate(Base):
    public_id = models.CharField(max_length=255, null=False, unique=True)
    avatar = models.ImageField(upload_to="avatar/")
    country = models.CharField(max_length=225, null=False)
    address = models.TextField(null=True)
    city = models.CharField(max_length=225, null=True)
    phone_number = PhoneNumberField(null=True)
    gender = models.IntegerField(
        choices=EnumGenderChoice.CHOICE, default=EnumGenderChoice.male)
    interest = models.ManyToManyField(Interest, related_name='interest_many')
    available = models.ManyToManyField(
        Available, related_name='available_many')
    link_web = models.URLField(null=True)

    def save(self, *args, **kwargs):
        self.update_at = timezone.now()
        super().save(*args, **kwargs)


class Follow(models.Model):
    author = models.ForeignKey(Authenticate, on_delete=models.CASCADE)
    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField()

    def save(self, *args, **kwargs):
        self.update_at = timezone.now()
        super().save(*args, **kwargs)


class Composite(models.Model):
    composite = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    follow = models.ManyToManyField(Follow, related_name="follow_many")
    followers = models.ManyToManyField(Follow, related_name="followers_many")
    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField()

    def save(self, *args, **kwargs):
        self.update_at = timezone.now()
        super().save(*args, **kwargs)
