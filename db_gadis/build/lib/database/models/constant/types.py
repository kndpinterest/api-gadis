from django.utils.translation import gettext_lazy as _


class EnumGenderChoice:
    male = 0
    female = 1
    CHOICE = (
        (male, _('Male')),
        (female, _('Female')))
