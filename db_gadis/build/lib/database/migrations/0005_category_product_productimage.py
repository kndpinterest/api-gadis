# Generated by Django 3.1.5 on 2021-02-04 08:00

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('database', '0004_auto_20210202_1827'),
    ]

    operations = [
        migrations.CreateModel(
            name='ProductImage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', models.ImageField(upload_to='product/')),
                ('create_at', models.DateTimeField(auto_now_add=True)),
                ('update_at', models.DateTimeField()),
            ],
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('public_id', models.CharField(max_length=225, unique=True)),
                ('name', models.CharField(max_length=225)),
                ('description', models.TextField()),
                ('sold_out', models.BooleanField(default=False)),
                ('price', models.DecimalField(decimal_places=2, max_digits=19)),
                ('total', models.IntegerField(default=0)),
                ('sold_total', models.IntegerField(default=0)),
                ('create_at', models.DateTimeField(auto_now_add=True)),
                ('update_at', models.DateTimeField()),
                ('author', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='database.authenticate')),
                ('image', models.ManyToManyField(related_name='image_product_many', to='database.ProductImage')),
            ],
        ),
        migrations.CreateModel(
            name='Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('public_id', models.CharField(max_length=225, unique=True)),
                ('name', models.CharField(max_length=225, unique=True)),
                ('create_at', models.DateTimeField(auto_now_add=True)),
                ('update_at', models.DateTimeField(auto_now_add=True)),
                ('author', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='author_category_product', to='database.authenticate')),
                ('product', models.ManyToManyField(related_name='product_many', to='database.Product')),
            ],
        ),
    ]
