from database.models.authenticate import Authenticate
from django.db import models
from django.utils import timezone


class Message(models.Model):
    author = models.ForeignKey(
        Authenticate, on_delete=models.CASCADE, related_name='message_author_relationship')
    message = models.TextField(null=False)
    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField()

    def save(self, *args, **kwargs):
        self.update_at = timezone.now()
        super().save(*args, **kwargs)


class Room(models.Model):
    public_id = models.CharField(max_length=225, null=False, unique=True)
    author = models.ForeignKey(
        Authenticate, on_delete=models.CASCADE, related_name='room_author_relationship')
    client = models.ForeignKey(
        Authenticate, on_delete=models.CASCADE, related_name='room_client_relationship')
    message = models.ManyToManyField(
        Message, related_name='message_many_to_many')
    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField()

    def save(self, *args, **kwargs):
        self.update_at = timezone.now()
        super().save(*args, **kwargs)
