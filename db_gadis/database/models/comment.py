from django.db import models
from django.utils import timezone


class Comment(models.Model):
    author = models.ForeignKey(
        'Authenticate', on_delete=models.CASCADE, unique=False, null=True)
    comment = models.TextField(null=False)
    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField()

    def save(self, *args, **kwargs):
        self.update_at = timezone.now()
        super().save(*args, **kwargs)

    class Meta:
        ordering = ['-create_at']
