from django.db import models
from django.contrib.auth.models import AbstractUser, UserManager
from django.utils.translation import gettext_lazy as _


class ManagerUser(UserManager):

    def create_user(self, email, password, **extra_field):
        if not email:
            raise ValueError(_('Email must be set up'))
        user = self.model(email=email, **extra_field)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, email, password, **extra_field):
        extra_field.setdefault('is_staff', True)
        extra_field.setdefault('is_superuser', True)

        if extra_field.get('is_staff') is not True:
            raise ValueError(_('Superuser must have is_staff=True.'))
        if extra_field.get('is_superuser') is not True:
            raise ValueError(_('Superuser must have is_superuser=True'))
        return self.create_user(email, password, **extra_field)


class CustomUser(AbstractUser):
    username = models.CharField(max_length=225, null=True, unique=True)
    email = models.EmailField(_('email address'), null=False, unique=True)
    first_name = models.CharField(max_length=225, null=True)
    last_name = models.CharField(max_length=225, null=True)
    date_joined = models.DateTimeField(auto_now_add=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []
    objects = ManagerUser()


class BaseSoft(models.Model):
    is_deleted = models.BooleanField(default=False)

    class Meta:
        abstract = True


class BaseTimeStamps(models.Model):
    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField()

    class Meta:
        abstract = True


class BaseForeignKey(models.Model):
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)

    class Meta:
        abstract = True


class Base(BaseSoft, BaseTimeStamps, BaseForeignKey):
    class Meta:
        abstract = True
