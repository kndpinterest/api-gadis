from database.models.message.room import Message, Room
from gadis_message.serializer.base import Base
from rest_framework import serializers
import uuid
from gadis_authenticate.serializer.user_child_serializer import AuthorModelSerializer


class MessageModelSerializer(serializers.ModelSerializer):
    author = AuthorModelSerializer(read_only=True)

    class Meta:
        model = Message
        fields = "__all__"


class RoomModelSerializer(serializers.ModelSerializer):
    author = AuthorModelSerializer(read_only=True)
    client = AuthorModelSerializer(read_only=True)
    message = MessageModelSerializer(read_only=True, many=True)

    class Meta:
        model = Room
        fields = "__all__"
