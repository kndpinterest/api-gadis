from rest_framework import serializers
from database.models.authenticate import Authenticate


class BaseRoom(serializers.Serializer):

    author = serializers.PrimaryKeyRelatedField(
        queryset=Authenticate.objects.all(), required=False)
    client = serializers.PrimaryKeyRelatedField(
        queryset=Authenticate.objects.all(), required=False)
    message = serializers.CharField(required=False)

    class Meta:
        abstract = True


class Base(BaseRoom):
    class Meta:
        abstract = True
