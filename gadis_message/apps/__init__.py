from django.apps import AppConfig


class GadisMessageConfig(AppConfig):
    name = 'gadis_message'
