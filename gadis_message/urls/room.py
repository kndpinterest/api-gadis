from gadis_message.views.message import MessageModelViewSet
from rest_framework import routers
from django.urls import path, include

router = routers.DefaultRouter()
router.register('', MessageModelViewSet, basename='message')

urlpatterns = [
    path('', include((router.urls, 'api-mes')))
]
