import logging
import unittest
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient
from database.models.authenticate import Authenticate
from rest_framework_jwt.settings import api_settings
from database.models.product import Product

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER


class TestsRoom(unittest.TestCase):
    def __init__(self, methodName):
        super(TestsRoom, self).__init__(methodName=methodName)
        self.author = Authenticate.objects.first()
        if self.author:
            self.payload = jwt_payload_handler(self.author.user)
            self.encode = jwt_encode_handler(self.payload)

    def setUp(self):
        self.e = APIClient()

    @unittest.skipIf(Authenticate.objects.count() == 0 or Product.objects.count() == 0, "Authenticate or Product not have data")
    def test_record_room(self):
        self.e.credentials(HTTP_AUTHORIZATION='Bearer ' + self.encode)
        product = Product.objects.last()
        urls = reverse('api-mes:message-detail',
                       args=[product.author.public_id])
        response = self.e.put(urls, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotEqual(response.data, None)
        log = logging.getLogger(__name__)
        log.info('Room has been created')
