import uuid
from database.models.authenticate import Authenticate
from rest_framework.generics import get_object_or_404
from gadis_message.serializer.message_serializer import MessageModelSerializer
from database.models.message.room import Room
from rest_framework import status, permissions
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response


class MessageModelViewSet(ModelViewSet):

    queryset = Room.objects.all()
    serializer_class = MessageModelSerializer

    def create(self, request):
        pass

    def update(self, request, pk):
        client = request.user.authenticate_set.first()

        author = get_object_or_404(Authenticate, public_id=pk)
        f_rom = Room.objects.filter(author=author).first()
        if not f_rom:
            f_rom = Room(public_id=str(uuid.uuid4()),
                         author=author, client=client)
            f_rom.save()
        return Response(f_rom.public_id)
