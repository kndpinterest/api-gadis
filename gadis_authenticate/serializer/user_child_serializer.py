from gadis_authenticate.serializer.available_serializer import AvailableSerializer
from database.models.base import CustomUser
from rest_framework import serializers
from database.models.authenticate import Authenticate, Interest
import os
import random
from django.conf import settings


class SafeUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = ['first_name', 'last_name']


class InterestModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Interest
        fields = "__all__"


class AuthorModelSerializer(serializers.ModelSerializer):
    interest = InterestModelSerializer(read_only=True, many=True)
    available = AvailableSerializer(read_only=True, many=True)

    class Meta:
        model = Authenticate
        fields = "__all__"

    avatar = serializers.SerializerMethodField("get_avatar_display")
    user = serializers.SerializerMethodField('get_user_display')

    def get_avatar_display(self, info):
        avatar = None
        try:
            avatar = info.avatar.url
            filename = str(avatar).split('/')
            path = 'assets/media/avatar/%s' % filename[len(filename) - 1]

            if not open(path):
                avatar = '%savatar_soft/%s' % (settings.MEDIA_URL, os.listdir(
                    'assets/media/avatar_soft')[random.randint(0, 1)])
        except ValueError:
            avatar = '%savatar_soft/%s' % (settings.MEDIA_URL, os.listdir(
                'assets/media/avatar_soft')[random.randint(0, 1)])

        except FileNotFoundError:
            avatar = '%savatar_soft/%s' % (settings.MEDIA_URL, os.listdir(
                'assets/media/avatar_soft')[random.randint(0, 1)])
        return avatar

    def get_user_display(self, info):
        serializer = SafeUserSerializer(info.user)
        return serializer.data
