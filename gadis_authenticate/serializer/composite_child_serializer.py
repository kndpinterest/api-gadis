from gadis_authenticate.serializer.user_child_serializer import AuthorModelSerializer
from rest_framework import serializers
from database.models.authenticate import Follow


class FollowSerializer(serializers.ModelSerializer):
    author = AuthorModelSerializer(read_only=True)

    class Meta:
        model = Follow
        fields = "__all__"

    has_follow = serializers.SerializerMethodField('get_has_follow_display')

    def get_has_follow_display(self, info):
        data = False
        try:
            if self.context.get('user') == info.author:  # Filter Followers From Author
                data = True
            else:
                try:
                    # Filter Follow From Client
                    composite = self.context.get(
                        'user').user.composite_set.first()
                    if composite.follow.filter(author=info.author).first():
                        data = True
                except AttributeError:
                    pass
        except KeyError:
            pass
        return data
