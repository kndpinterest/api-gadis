from database.models.authenticate import Interest
from gadis_authenticate.serializer.user_child_serializer import AuthorModelSerializer
from gadis_authenticate.serializer.composite_serializer import CompositeSerializer
import uuid
import os
import dotenv
from django.core.mail import EmailMessage
from django.utils.translation import gettext as _
from rest_framework import serializers
from database.models.base import CustomUser
from gadis_authenticate.serializer.available_serializer import Available, BaseAvailableSerializer

from gadis_authenticate.serializer.base import BaseAuthenticateSerializer, BaseInterestSerializer, BaseUserSerializer

from rest_framework_jwt.settings import api_settings

dotenv.load_dotenv()

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER


class Base(BaseAuthenticateSerializer, BaseInterestSerializer, BaseUserSerializer, BaseAvailableSerializer):
    class Meta:
        abstract = True


class UserSerializer(Base):
    def __init__(self, instance=None, data=None, *args, **kwargs):
        super(UserSerializer, self).__init__(
            instance=instance, data=data, *args, **kwargs)

    def get_fields(self, *args, **kwargs):
        fields = super(UserSerializer, self).get_fields(*args, **kwargs)
        try:
            if self.context['update']:
                if self.context['args'] == 'available':
                    fields['available_name'].required = True

        except KeyError:
            fields['email'].required = True
            fields['password'].required = True
            fields['confirm_password'].required = True
            if self.context['args']:
                fields['password'].required = False
                fields['confirm_password'].required = False
            try:
                if self.context['authenticate']:
                    fields['email'].required = False
            except KeyError:
                pass

        return fields

    def create(self, validated_data):

        check = CustomUser.objects.filter(
            email=validated_data.get('email')).first()
        try:
            if self.context['authenticate']:
                check = CustomUser.objects.filter(
                    email=self.context['authenticate']).first()
        except KeyError:
            pass
        if not check:
            if validated_data.get('password') != validated_data.get('confirm_password'):
                raise serializers.ValidationError(
                    {'message': _("Password don't match")})

            user = CustomUser(email=validated_data.get(
                'email'), password=validated_data.get('password'))
            user.set_password(validated_data.get('password'))
            user.save()
            user.composite_set.create(composite=user)

            # JWT Token
            user.authenticate_set.create(public_id=str(uuid.uuid4()))
            payload = jwt_payload_handler(user)
            token = jwt_encode_handler(payload)
            self.context['token'] = token
            return self.context
        if self.context['args']:
            author = check.authenticate_set.first()
            if self.context['args'] == 'v1':
                self.upload_avatar(author, validated_data)  # Upload Avatar
                author.avatar = validated_data.get('avatar')
            self.change_nickname(check, validated_data, self)
            self.push_interest(author, validated_data)
            author.save()
            return self.context
        raise serializers.ValidationError(
            {'message': _('Email already exists, please choose another one.')})

    @classmethod
    def push_interest(cls, args, extra_fields):
        try:
            check = args.interest.all()
            if check:
                check.delete()
            for i, c in enumerate(extra_fields.get('interest').split('#')):
                interst = Interest(name=c)
                interst.save()
                args.interest.add(interst)
        except AttributeError:
            pass
        return args

    @classmethod
    def change_nickname(cls, args, extra_fields, extra_self):
        extra_self.context['user'] = args.email
        try:
            nickname = extra_fields.get('nickname').split(' ')
            args.first_name = nickname[0]
            if(len(nickname) >= 2):
                newNickname = ''
                for i, c in enumerate(nickname):
                    if i == 1:
                        continue
                    newNickname += ' %s' % c
                args.last_name = newNickname
        except AttributeError:
            pass
        args.save()
        return args

    @classmethod
    def upload_avatar(cls, args, extra_fields):
        try:
            # Upload Avatar
            if args.avatar.uri:
                split = str(args.avatar.uri).split('/')
                remove = split[len(split) - 1]
                os.system('rm assets/media/avatar/%s' % remove)
        except AttributeError:
            pass
        return args

    # Update Data

    def update(self, instance, validated_data):
        if self.context['args'] == 'available':
            self.retrieve_available_product(
                instance, validated_data)  # Update Available Product
            return self.retrieve_available_product
        return instance

    @classmethod
    def retrieve_available_product(cls, args, extra_fields):
        author = args.authenticate_set.first()
        available = author.available
        filter_av = available.all().filter(name=extra_fields.get('available_name')).first()
        if filter_av:
            available.remove(filter_av)
        else:
            c_av = Available(name=extra_fields.get('available_name'))
            c_av.save()
            available.add(c_av)
        return available

    @classmethod
    def reset_password(cls, args, validated_data):
        mail = EmailMessage("Subject", "Objects",
                            os.environ.get('xxx'), [args.email])
        mail.send()
        return mail


class UserModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = ['author', 'first_name', 'last_name',
                  'date_joined', 'id', 'composite', 'has_follow']
    author = serializers.SerializerMethodField('get_author_display')
    composite = serializers.SerializerMethodField('get_composite_display')
    has_follow = serializers.SerializerMethodField('get_has_follow_display')

    def get_has_follow_display(self, info):
        composite = info.composite_set.first()
        data = None
        try:
            if composite.followers.filter(author=self.context.get('user')).first():
                data = "Unfollow"
            else:
                data = "Follow"
        except KeyError:
            pass
        if self.context.get('user') == info.authenticate_set.first():
            data = False
        return data

    def get_composite_display(self, info):
        serializer = CompositeSerializer(info.composite_set.first())
        try:
            serializer.context['user'] = self.context.get('user')
        except KeyError:
            pass
        return serializer.data

    def get_author_display(self, info):
        serializer = AuthorModelSerializer(info.authenticate_set.first())
        return serializer.data
