from gadis_authenticate.serializer.composite_child_serializer import FollowSerializer
from database.models.authenticate import Composite
from rest_framework import serializers


class CompositeSerializer(serializers.ModelSerializer):
    def __init__(self, *args, **kwargs):
        super(CompositeSerializer, self).__init__(*args, **kwargs)

    follow = FollowSerializer(read_only=True, many=True)
    followers = FollowSerializer(read_only=True, many=True)

    class Meta:
        model = Composite
        fields = "__all__"

    ttl_ffw = serializers.SerializerMethodField('get_ttl_ff_display')
    ttl_fs = serializers.SerializerMethodField('get_ttl_fs_display')

    def get_ttl_ff_display(self, info):
        return info.follow.count()

    def get_ttl_fs_display(self, info):
        return info.followers.count()
