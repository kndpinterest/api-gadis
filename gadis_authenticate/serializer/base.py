from database.models.constant.types import EnumGenderChoice
from rest_framework import serializers


class BaseUserSerializer(serializers.Serializer):
    username = serializers.CharField(required=False)
    email = serializers.EmailField(required=False)
    first_name = serializers.CharField(required=False)
    last_name = serializers.CharField(required=False)
    old_password = serializers.CharField(required=False)
    password = serializers.CharField(required=False)
    confirm_password = serializers.CharField(required=False)
    token = serializers.CharField(required=False)

    class Meta:
        abstract = True


class BaseAuthenticateSerializer(serializers.Serializer):
    nickname = serializers.CharField(required=False)
    avatar = serializers.ImageField(required=False)
    country = serializers.CharField(required=False)
    address = serializers.CharField(required=False)
    city = serializers.CharField(required=False)
    gender = serializers.IntegerField(default=EnumGenderChoice.male)
    interest = serializers.CharField(required=False)

    class Meta:
        abstract = True


class BaseInterestSerializer(serializers.Serializer):
    name = serializers.CharField(required=False)

    class Meta:
        abstract = True
