from database.models.authenticate import Available
from rest_framework import serializers


class BaseAvailableSerializer(serializers.Serializer):
    available_name = serializers.CharField(required=False)

    class Meta:
        abstract = True


class AvailableSerializer(serializers.ModelSerializer):
    class Meta:
        model = Available
        fields = ['name', ]
