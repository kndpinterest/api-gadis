from gadis_authenticate.serializer.composite_child_serializer import FollowSerializer
from django.utils.translation import gettext as _
from rest_framework import status, permissions, parsers
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from database.models.authenticate import Composite, Follow
from database.models.authenticate import Authenticate


class FollowAPIView(APIView):
    permission_classes = [permissions.IsAuthenticated, ]
    serializer_class = FollowSerializer

    def post(self, request, public_id):
        # Client
        client = request.user
        composite = client.composite_set.first()
        author_client = client.authenticate_set.first()

        # Author
        author = Authenticate.objects.filter(public_id=public_id).first()
        composite_author = author.user.composite_set.first()

        # Filter Follow From Client
        follow = composite.follow.all().filter(author=author).first()
        if follow:
            # Remove Follow From Client
            composite.follow.remove(follow)

            # Remove Followers From Author
            followers = composite_author.followers.all().filter(author=author_client).first()
            composite_author.followers.remove(followers)
            return Response(True, status=status.HTTP_200_OK)
        else:
            # Add Followers To Author
            followers = Follow(author=author_client)
            followers.save()
            composite_author.followers.add(followers)

            # Add Follow To Client
            follow = Follow(author=author)
            follow.save()
            composite.follow.add(follow)
            return Response(True, status=status.HTTP_200_OK)
