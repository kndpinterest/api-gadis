from gadis_authenticate.serializer.user_serializer import UserSerializer
from rest_framework import status, permissions, parsers
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response


class AvailableAPIView(APIView):
    permission_classes = [permissions.IsAuthenticated, ]
    serializer_class = UserSerializer

    def post(self, request):
        user = request.user
        serializer = self.serializer_class(user, data=request.data)
        serializer.context['update'] = 'available'
        serializer.context['args'] = 'available'
        if serializer.is_valid():
            serializer.save()
            return Response(True, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
