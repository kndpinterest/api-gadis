import os
from django.utils.translation import gettext as _
import dotenv
from django.shortcuts import get_object_or_404
from rest_framework import status, permissions, parsers
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from database.models.base import CustomUser
from gadis_authenticate.serializer.user_serializer import UserSerializer, UserModelSerializer
from django.core.mail import EmailMessage
from database.models.authenticate import Authenticate
from core.pagination.paginate import CustomPagination
from rest_framework.generics import ListAPIView
from django_filters.rest_framework import DjangoFilterBackend
from gadis_authenticate.backend.filter_user import UserFilterSet

dotenv.load_dotenv()


class UserGenericListAPIView(ListAPIView):
    queryset = CustomUser.objects.all()
    serializer_class = UserModelSerializer
    pagination_class = CustomPagination
    permission_classes = [permissions.AllowAny, ]
    filter_backends = [DjangoFilterBackend, ]
    filterset_class = UserFilterSet

    def get_queryset(self):
        if self.request.user.is_authenticated:
            return self.queryset.exclude(id=self.request.user.id)
        return self.queryset


class UserModelViewSet(ModelViewSet):
    queryset = CustomUser.objects.all()
    serializer = UserSerializer
    serializer_class = UserModelSerializer
    parser_classes = [parsers.JSONParser, parsers.MultiPartParser, ]

    def get_permissions(self):
        if self.action == 'create':
            permission_classes = [permissions.AllowAny, ]
        elif self.action == "retrieve":
            permission_classes = [permissions.AllowAny, ]
        else:
            permission_classes = [permissions.IsAuthenticated, ]
        return [permission() for permission in permission_classes]

    def create(self, request):
        serializer = self.serializer(data=request.data)
        if request.user.is_authenticated:
            user = request.user
            serializer.context['authenticate'] = user.email
        serializer.context['args'] = request.data.get('args')
        if serializer.is_valid():
            serializer.save()
            try:
                return Response({'token': serializer.context['token']}, status=status.HTTP_201_CREATED)
            except KeyError:
                try:
                    user = CustomUser.objects.filter(
                        email=serializer.context['user']).first()
                    serializers = self.serializer_class(user)
                    return Response(serializers.data, status=status.HTTP_201_CREATED)
                except KeyError:
                    pass
            return Response(True, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def retrieve(self, request, pk):
        author = Authenticate.objects.filter(public_id=pk).first()
        serializer = self.serializer_class(author.user)
        if(request.user.is_authenticated):
            serializer.context['user'] = request.user.authenticate_set.first()
        return Response(serializer.data, status=status.HTTP_200_OK)


class MeAPIView(APIView):
    permission_classes = [permissions.IsAuthenticated, ]
    serializer_class = UserModelSerializer

    def get(self, request):
        serializer = self.serializer_class(request.user)
        return Response(serializer.data, status=status.HTTP_200_OK)


class ResetPasswordAPIView(APIView):
    permission_classes = [permissions.AllowAny, ]
    serializer_class = UserSerializer

    def post(self, request):
        check = CustomUser.objects.filter(
            email=request.data.get('email')).first()
        if not check:
            return Response({'message': _('Accounts not found')}, status=status.HTTP_400_BAD_REQUEST)
        mail = EmailMessage(
            "OBjects", "OB", os.environ.get('xxx'), [check.email])
        mail.send()
        return Response({'message': 'Reset'}, status=status.HTTP_200_OK)
