from rest_framework import routers
from django.urls import path, include
from gadis_authenticate.views.user import MeAPIView, UserModelViewSet, ResetPasswordAPIView, UserGenericListAPIView
from gadis_authenticate.views.composite import FollowAPIView
from gadis_authenticate.views.available import AvailableAPIView

router = routers.DefaultRouter()
router.register('', UserModelViewSet, basename='user')

urlpatterns = [
    path('', include((router.urls, 'api'))),
    path('accounts/list/', UserGenericListAPIView.as_view(), name='user-list'),
    path('accounts/me/', MeAPIView.as_view(), name='me'),
    path('accounts/reset/pasword/',
         ResetPasswordAPIView.as_view(), name='reset-user'),
    path('accounts/client/follow/<public_id>/',
         FollowAPIView.as_view(), name='follow-user'),
    path('accounts/add/available/product/',
         AvailableAPIView.as_view(), name='available-user')
]
