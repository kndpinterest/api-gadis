import unittest
import logging
import random
import string
from rest_framework.test import APIClient
from rest_framework import status
from database.models.base import CustomUser
from django.urls import reverse
from django.core.files import File
from database.models.authenticate import Authenticate

from rest_framework_jwt.settings import api_settings

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

def rand_name():
    strings = string.ascii_lowercase
    return ''.join(random.choice(strings) for i in range(6, 20))


rand_mail = ['@yahoo.com', '@example.com',
             '@hotmail.com', '@cloud.com', '@gmail.com']
rand_nickname = [
    'Chrollo Lucifer',
    'Debora Natalia Hernández Lara',
    'Awootistic',
    'Rebeka Martinez'
]

hobby = [
    "Introver Drink", "Yoga Dancing", "DJ Music Sex", "Fighting Karate Coding Food Chef"
]

available_product = ['chiken','fish','snakes','lobster','bird','vegetable']

class TestsUser(unittest.TestCase):
    def __init__(self, methodName):
        super(TestsUser, self).__init__(methodName=methodName)
        self.user = CustomUser.objects.first()
        if self.user:
            self.token = jwt_payload_handler(self.user)
            self.encode = jwt_encode_handler(self.token)

        self.user_another = authtoken = CustomUser.objects.filter(
            first_name__isnull=False, last_name__isnull=False).last()

        if self.user_another:
            self.token_another = jwt_payload_handler(self.user_another)
            self.encode_another = jwt_encode_handler(self.token_another)


    def setUp(self):
        self.e = APIClient()

    def test_record_user(self):
        urls = reverse('api:user-list')
        data = {
            'email': '%s%s' % (rand_name(), rand_mail[random.randint(0, 4)]),
            'password': 'Username@123',
            'confirm_password': 'Username@123'
        }
        response = self.e.post(urls, data, format='multipart')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertNotEqual(response.data, None)
        log = logging.getLogger(__name__)
        log.info('Record User')

    @unittest.skipIf(Authenticate.objects.count() == 0, "Authenticate not have data")
    def test_record_avatar(self):
        urls = reverse('api:user-list')
        author = Authenticate.objects.last()
        data = {
            'avatar': File(open('assets/43383bbe7049f170fc60a50068af16e4.jpg', 'rb')),
            'email': author.user.email,
            'args': 'v1'
        }
        response = self.e.post(urls, data, format='multipart')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertNotEqual(response.data, None)
        log = logging.getLogger(__name__)
        log.info('Record Avatar')

    @unittest.skipIf(CustomUser.objects.count() == 0, "Authenticate not have data")
    def test_change_nickname(self):
        self.e.credentials(HTTP_AUTHORIZATION='Bearer ' + self.encode)

        urls = reverse('api:user-list')
        data = {
            'nickname': rand_nickname[random.randint(0, 3)],
            'args': 'v1plus'
        }
        response = self.e.post(urls, data, format='multipart')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertNotEqual(response.data, None)
        log = logging.getLogger(__name__)
        log.info('Change Nickname')

    @unittest.skipIf(Authenticate.objects.count() == 0, "Authenticate not have data")
    def test_push_interest(self):
        self.e.credentials(HTTP_AUTHORIZATION='Bearer ' + self.encode)
        urls = reverse('api:user-list')

        author = Authenticate.objects.filter(interest__isnull=True).first()
        data = {
            'interest': hobby[random.randint(0, 3)],
            'email': author.user.email,
            'args': 'v2'
        }
        response = self.e.post(urls, data, format='multipart')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertNotEqual(response.data, None)
        log = logging.getLogger(__name__)
        log.info('Push Interest')

    def test_user_list(self):
        self.e.credentials(HTTP_AUTHORIZATION='Bearer ' + self.encode)

        urls = reverse('api:user-list')
        response = self.e.get(urls, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotEqual(response.data, None)
        log = logging.getLogger(__name__)
        log.info('List User total %s' % len(response.data))

    @unittest.skipIf(CustomUser.objects.filter(first_name__isnull=False, last_name__isnull=False).count() == 0, "User not have data")
    def test_user_detail(self):
        authtoken = CustomUser.objects.filter(
            first_name__isnull=False, last_name__isnull=False).last()

        self.e.credentials(HTTP_AUTHORIZATION='Bearer ' + self.encode_another)

        urls = reverse('api:user-detail', args=[authtoken.authenticate_set.first().public_id])
        response = self.e.get(urls, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotEqual(response.data, None)
        log = logging.getLogger(__name__)
        log.info('User Detail')

    @unittest.skipIf(CustomUser.objects.count() == 0, "User not have data")
    def test_reset_user(self):
        user = CustomUser.objects.first()
        urls = reverse('reset-user')
        data = {
            'email': user.email}
        response = self.e.post(urls, data, format='multipart')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotEqual(response.data, None)
        log = logging.getLogger(__name__)
        log.info('Reset User')

    @unittest.skipIf(CustomUser.objects.count() == 0, "User not have data")
    def test_user_me(self):
        self.e.credentials(HTTP_AUTHORIZATION='Bearer ' + self.encode_another)

        urls = reverse('me')
        response = self.e.get(urls, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotEqual(response.data, None)
        log = logging.getLogger(__name__)
        log.info('User Me')

    @unittest.skipIf(Authenticate.objects.count() <= 2, "Authenticate not have data")
    def test_follow_user(self):
        self.e.credentials(HTTP_AUTHORIZATION='Bearer ' + self.encode)

        client = Authenticate.objects.last()
        urls = reverse('follow-user', args=[client.public_id])

        response = self.e.post(urls, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotEqual(response.data, None)
        log = logging.getLogger(__name__)
        log.info('Follow User')

    @unittest.skipIf(Authenticate.objects.count() == 0,"Authenticate not have data")
    def test_available_product(self):
        self.e.credentials(HTTP_AUTHORIZATION='Bearer ' + self.encode)

        urls = reverse('available-user')
        data = {
                'available_name': available_product[random.randint(0,5)]}
        response = self.e.post(urls, data, format='json')
        print(response.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotEqual(response.data, None)
        log = logging.getLogger(__name__)
        log.info('Available Product Record and Remove')
