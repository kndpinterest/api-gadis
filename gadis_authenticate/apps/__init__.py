from django.apps import AppConfig


class GadisAuthenticateConfig(AppConfig):
    name = 'gadis_authenticate'
