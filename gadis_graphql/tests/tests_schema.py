from graphene.test import Client
import logging
import unittest
from gadis_graphql.store.configureStore import schema


class TestsSchema(unittest.TestCase):
    def setUp(self):
        self.e = Client(schema=schema)

    def test_schema(self):
        response = self.e.execute(
            '''
            query {
  general {
    touch_options_result {
      follow
      unfollow
      share
      report
      block
    }
    touch_result {
      signin
      signup
      forgot
      already
      create
      or_
      facebook
      google
      find
      next
      done
      follow
      unfollow
      followers
      follow_
      follow_you
    }
    touch_drawer_result {
      home
      find_seller
      general
      view
      search
      child {
        name
      }
    }
    touch_tab_result {
      name
    }
    validation {
      validation_find_seller
    } 
    touch_tab_result {
      name
    }
  }
}
                '''
        )
        self.assertNotEqual(response, None)
        log = logging.getLogger(__name__)
        log.info(response)

    def test_product_list_graphql(self):
        response = self.e.execute('''
        query {
  all_products (name__icontains: "green") {
    edges {
      node {
        id
        name
        price
        sold_out
        sold_total
        description
        author {
          avatar
          user {
            first_name
            last_name
            username
            email
            date_joined
          }
        }
        update_at
        create_at
      }
    }
  }
}
        ''')
        log = logging.getLogger(__name__)
        log.info('Graphql Product List %s' % response)
