from gadis_graphql.mutations.message_mutations import RoomNode
from gadis_graphql.mutations.product_mutation import ProductNode
from graphene_django.filter import DjangoFilterConnectionField

from graphene import relay
from gadis_graphql.constant.general_types import GeneralTypes
from gadis_graphql.constant.interface import InterfaceTypes
import graphene
from graphql_jwt.decorators import login_required
from gadis_graphql.mutations.auth_mutations import AuthenticateJWT
from gadis_graphql.constant.user_node import UserNode


class Query(graphene.ObjectType):
    user = graphene.Field(UserNode)
    field = graphene.Field(InterfaceTypes)
    general = graphene.Field(GeneralTypes)

    products = relay.Node.Field(ProductNode)
    all_products = DjangoFilterConnectionField(ProductNode)

    message = relay.Node.Field(RoomNode)
    all_message = DjangoFilterConnectionField(RoomNode)

    def resolve_general(self, info):
        return info

    def resolve_field(self, info):
        return info

    @login_required
    def resolve_user(self, info):
        return info


schema = graphene.Schema(auto_camelcase=False,
                         query=Query, mutation=AuthenticateJWT)
