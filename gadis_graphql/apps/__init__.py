from django.apps import AppConfig


class GadisGraphqlConfig(AppConfig):
    name = 'gadis_graphql'
