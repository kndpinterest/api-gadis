import graphene
from graphql_jwt import relay as rl
from graphene import relay
from database.models.base import CustomUser
from database.models.authenticate import Authenticate
from graphene_django import DjangoObjectType
from graphene_django.filter import DjangoFilterConnectionField


class UserTypes(DjangoObjectType):
    class Meta:
        model = CustomUser
        filter_fields = {
            "username": ["exact", "icontains", "istartswith", ],
            "email": ["icontains", ],
            "first_name": ["icontains", ],
            "last_name": ["icontains", ]
        }
        interfaces = (relay.Node,)


class AuthTypes(DjangoObjectType):
    class Meta:
        model = Authenticate
        filter_fields = {
            'user__username': ['exact', 'icontains', ],
            'user__first_name': ['exact', 'icontains', ],
            'user__last_name': ['exact', 'icontains', ]
        }
        interfaces = (relay.Node,)


class UserNode(graphene.ObjectType):
    result_user = relay.Node.Field(UserTypes)
    result_all_user = DjangoFilterConnectionField(UserTypes)

    result_author = relay.Node.Field(AuthTypes)
    result_all_author = DjangoFilterConnectionField(AuthTypes)
