import graphene
import json
from django.utils.translation import gettext as _

with open('prefix/rn.menu.child.json', 'r') as r:
    rn = r.read()

with open('prefix/rn.tab.json', 'r') as r:
    touch = r.read()


class ValidationRN(graphene.ObjectType):
    validation_find_seller = graphene.String()


class CredentialsRN(graphene.ObjectType):
    title_login = graphene.String()
    title_register = graphene.String()
    title_forgot = graphene.String()
    title_avatar = graphene.String()
    title_interest = graphene.String()
    title_cong = graphene.String()
    copyright = graphene.String()


class TouchRN(graphene.ObjectType):
    signin = graphene.String()
    signup = graphene.String()
    forgot = graphene.String()
    already = graphene.String()
    create = graphene.String()
    or_ = graphene.String()
    facebook = graphene.String()
    google = graphene.String()
    find = graphene.String()
    next = graphene.String()
    done = graphene.String()
    follow = graphene.String()
    unfollow = graphene.String()
    followers = graphene.String()
    follow_ = graphene.String()
    follow_you = graphene.String()
    back = graphene.String()
    message = graphene.String()
    date_joined = graphene.String()
    save = graphene.String()
    buy = graphene.String()


class TouchTabRN(graphene.ObjectType):
    name = graphene.String()


class TouchDrawrChildRN(graphene.ObjectType):
    name = graphene.String()


class TouchDrawerRN(graphene.ObjectType):
    home = graphene.String()
    find_seller = graphene.String()
    general = graphene.String()
    view = graphene.String()
    search = graphene.String()
    child = graphene.List(TouchDrawrChildRN)

    def resolve_child(self, info):
        return [TouchDrawrChildRN(name=i['name']) for i in json.loads(rn)]


class OptionsProfilRN(graphene.ObjectType):
    follow = graphene.String()
    unfollow = graphene.String()
    share = graphene.String()
    report = graphene.String()
    block = graphene.String()


class GeneralTypes(graphene.ObjectType):
    touch_tab_result = graphene.List(TouchTabRN)
    touch_drawer_result = graphene.Field(TouchDrawerRN)
    credentials_title_result = graphene.Field(CredentialsRN)
    touch_result = graphene.Field(TouchRN)
    touch_options_result = graphene.Field(OptionsProfilRN)
    validation = graphene.Field(ValidationRN)

    def resolve_validation(self, info):
        return ValidationRN(
            validation_find_seller=_("We didn't find any results Make sure everything is spelled correctly or try different keywords."))

    def resolve_touch_options_result(self, info):
        return OptionsProfilRN(
            follow=_("Follow"),
            unfollow=_("Unfollow"),
            share=_("Share"),
            report=_("Report"),
            block=_("Block")
        )

    def resolve_touch_result(self, info):
        return TouchRN(
            signin=_("Sign in"),
            signup=_("Sign up"),
            forgot=_("Forgotted password?"),
            create=_("Create new accounts?"),
            already=_("Already exists accounts?"),
            or_=_("Or"),
            facebook=_('Facebook'),
            google=_("Google"),
            find=_("Find"),
            done=_("Done"),
            next=_("Next"),
            follow=_("Following"),
            unfollow=_("Unfollow"),
            followers=_("Followers"),
            follow_=_("Follow"),
            follow_you=_("Follow you"),
            back=_("Back"),
            message=_("Message"),
            date_joined=_("Date Joined : "),
            save=_("Save"),
            buy=_("Buy")
        )

    def resolve_credentials_title_result(self, info):
        return CredentialsRN(
            title_login=_("Sign in to Gadis"),
            title_register=_("Join to gadis"),
            title_forgot=_("Reset your password"),
            title_avatar=_(
                "Some of these forms are to make it easier for others to get to know you and launch your sales"),
            title_interest=_(
                "Choose whatever you want to see, the selected one will be shown on the main screen for you"),
            title_cong=_("Congratulations you have successfully joined Gadis"),
            copyright=_("Copyright © 2021 Gadis")
        )

    def resolve_touch_drawer_result(self, info):
        return TouchDrawerRN(home=_("Home"), search=_("Search"), find_seller=_("Find a seller"), general=_("General"), view=_("view"))

    def resolve_touch_tab_result(self, info):
        return [TouchTabRN(name=i['name']) for i in json.loads(touch)]
