import graphene
import pycountry
from django.utils.translation import gettext as _
from database.models.constant.types import EnumGenderChoice
from django.conf import settings
import json

with open('prefix/animal.json', 'r') as r:
    interest = r.read()


class User(graphene.ObjectType):
    username = graphene.String()
    email = graphene.String()
    first_name = graphene.String()
    last_name = graphene.String()
    password = graphene.String()
    confirm_password = graphene.String()
    token = graphene.String()


class GenederChoice(graphene.ObjectType):
    label = graphene.String()
    value = graphene.String()


class Country(graphene.ObjectType):
    name = graphene.String()


class Interest(graphene.ObjectType):
    name = graphene.String()
    icon = graphene.String()


class Authenticate(graphene.ObjectType):
    avatar = graphene.String()
    nickname = graphene.String()
    country = graphene.String()
    country_list = graphene.List(Country)
    states = graphene.String()
    city = graphene.String()
    gender = graphene.String()
    gender_choice = graphene.List(GenederChoice)
    phone_number = graphene.String()
    interest = graphene.List(Interest)

    def resolve_interest(self, info):
        return [Interest(name=i['name'], icon='%sanimal/%s' % (settings.MEDIA_URL, i['icon'])) for i in json.loads(interest)]

    def resolve_gender_choice(self, info):
        return [GenederChoice(
            label=i[1], value=i[0]) for i in EnumGenderChoice.CHOICE]

    def resolve_country_list(self, info):
        return [Country(name=i.name) for i in list(pycountry.countries)]


class InterfaceTypes(graphene.ObjectType):
    result_user = graphene.Field(User)
    result_auth = graphene.Field(Authenticate)

    def resolve_result_user(self, info):
        return User(
            username=_("Username"),
            email=_("Email"),
            first_name=_("First name"),
            last_name=_("Last name"),
            password=_("Password"),
            confirm_password=_("Confirm Password"),
            token=_("Find username, email or phone number")
        )

    def resolve_result_auth(self, info):
        return Authenticate(
            nickname=_("Nickname"),
            avatar=_("Avatar"),
            country=_("Country"),
            states=_("States"),
            city=_("City"),
            gender=_("Gender"),
            phone_number=_("Phone number")
        )
