import graphene


class User(graphene.ObjectType):
    username = graphene.String()
    email = graphene.String()
    first_name = graphene.String()
    last_name = graphene.String()
    password = graphene.String()
    confirm_password = graphene.String()
    token = graphene.String()
