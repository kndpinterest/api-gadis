import graphene
import graphql_jwt


class AuthenticateJWT(graphene.ObjectType):
    token_auth = graphql_jwt.ObtainJSONWebToken.Field()
    verify_token = graphql_jwt.Verify.Field()
    refresh_token = graphql_jwt.Refresh.Field()
    revoked = graphql_jwt.Revoke.Field()
