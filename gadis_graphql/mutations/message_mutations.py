
from database.models.message.room import Room
from graphene import relay
from graphene_django import DjangoObjectType


class RoomNode(DjangoObjectType):
    class Meta:
        model = Room
        fields = "__all__"
        filter_fields = {
            'public_id': ['icontains', ]
        }
        interfaces = (relay.Node,)

    @classmethod
    def mutate(cls, root, info, text, id):
        pass
