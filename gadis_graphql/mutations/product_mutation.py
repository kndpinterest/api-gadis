from database.models.product import Product
from graphene import relay
from graphene_django import DjangoObjectType


class ProductNode(DjangoObjectType):
    class Meta:
        model = Product
        fields = "__all__"
        filter_fields = {
            'name': ['icontains', ],
        }

        interfaces = (relay.Node,)
