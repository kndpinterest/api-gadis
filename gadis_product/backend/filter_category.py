from django_filters import rest_framework as filters
from database.models.category import Category


class CategoryFilterSets(filters.FilterSet):
    name = filters.CharFilter(field_name='name', lookup_expr='icontains')

    class Meta:
        model = Category
        fields = ['name', ]
