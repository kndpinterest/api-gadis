from django_filters import rest_framework as filters
from database.models.product import Product


class ProductFilterSets(filters.FilterSet):
    name = filters.CharFilter(field_name='name', lookup_expr='icontains')

    class Meta:
        model = Product
        fields = ['name']
