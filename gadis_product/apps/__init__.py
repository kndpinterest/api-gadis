from django.apps import AppConfig


class GadisProductConfig(AppConfig):
    name = 'gadis_product'
