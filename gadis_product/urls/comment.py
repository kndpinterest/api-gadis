from django.urls import path, include
from gadis_product.views.comment import CommentModelViewSets
from rest_framework import routers

router = routers.DefaultRouter()
router.register('comment', CommentModelViewSets, basename='comment')

urlpatterns = [
    path('', include((router.urls, 'api-com')))
]
