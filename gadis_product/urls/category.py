from gadis_product.views.category import CategoryModelViewSet
from django.urls import path, include
from rest_framework import routers

router = routers.DefaultRouter()
router.register('category', CategoryModelViewSet, basename='categorys')

urlpatterns = [
    path('', include((router.urls, 'api-c'))),
    path('', include('gadis_product.urls.product')),
    path('', include('gadis_product.urls.comment'))
]
