from django.urls import path
from gadis_product.views.product import ProductGeenricRetrieveAPIView, ProductGenericListAPIView, CacheAlreadyProduct, RecordUUIDAPIView, SupportProductAPIView

urlpatterns = [
    path('list/', ProductGenericListAPIView.as_view(), name='product-list'),
    path('detail/<public_id>/',
         ProductGeenricRetrieveAPIView.as_view(), name='product-detail'),
    path('cache/product/image/', CacheAlreadyProduct.as_view(),
         name='cache-product-image'),
    path('record/uuid/product/', RecordUUIDAPIView.as_view(), name='uuid-product'),
    path('support/product/<public_id>/',
         SupportProductAPIView.as_view(), name='support-product')
]
