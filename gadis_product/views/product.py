import uuid
import os
from django.shortcuts import get_object_or_404
from rest_framework import status, permissions
from rest_framework.generics import ListAPIView, RetrieveAPIView
from database.models.product import Product, SoftImageForRemove, SupportProduct
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.response import Response
from rest_framework.views import APIView
from gadis_product.serializer.product_serializer import ProductModelSerializer
from gadis_product.backend.filter_product import ProductFilterSets
from gadis_authenticate.serializer.user_child_serializer import AuthorModelSerializer


class ProductGenericListAPIView(ListAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductModelSerializer
    filter_backends = [DjangoFilterBackend, ]
    filterset_class = ProductFilterSets
    permission_classes = [permissions.AllowAny, ]

    def get_queryset(self):
        return self.queryset


class ProductGeenricRetrieveAPIView(RetrieveAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductModelSerializer
    permission_classes = [permissions.AllowAny, ]

    def retrieve(self, request, public_id):
        product = self.get_queryset().filter(public_id=public_id).first()
        serializer = self.serializer_class(product)
        return Response(serializer.data, status=status.HTTP_200_OK)


class CacheAlreadyProduct(APIView):
    permission_classes = [permissions.AllowAny, ]

    def get(self, request):
        image = SoftImageForRemove.objects.filter(
            already_product__isnull=True)
        for i in image:
            for im in i.image.all():
                length = im.image.url.split('/')
                os.system('rm assets/media/product/%s' %
                          str(length[len(length) - 1]))
            i.delete()
        return Response(True, status=status.HTTP_200_OK)


class RecordUUIDAPIView(APIView):

    def get(self, request):
        return Response(uuid.uuid4(), status=status.HTTP_200_OK)


class SupportProductAPIView(APIView):
    queryset = SupportProduct.objects.all()
    serializer = AuthorModelSerializer
    # serializer = SupportModelSerializer

    def post(self, request, public_id):
        # Author
        product = get_object_or_404(Product, public_id=public_id)

        # Client
        user = request.user.authenticate_set.first()
        c_support = product.support.all().filter(author=user).first()
        if c_support:
            # Remove Support
            product.support.remove(c_support)
            c_support.delete()
            print("UNGIVE")
            return Response({'types': False, 'support': user.public_id}, status=status.HTTP_200_OK)
        else:
            print("GIVE")
            # Add Support Product
            c_s = SupportProduct(author=user)
            c_s.save()
            product.support.add(c_s)
            serializer = self.serializer(user)
            return Response({
                'types': True, 'support': serializer.data}, status=status.HTTP_200_OK)
