from database.models.product import Product
from gadis_product.serializer.product_serializer import ProductModelSerializer
from rest_framework import status, permissions, parsers
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from gadis_product.serializer.category_serializer import BaseCategorySerializer, CategoryModelSerializer
from database.models.category import Category


class CategoryModelViewSet(ModelViewSet):
    queryset = Category.objects.all()
    queryset_prod = Product.objects.all()
    serializer_class = CategoryModelSerializer
    serializer = BaseCategorySerializer
    serializer_product = ProductModelSerializer
    parser_classes = [parsers.JSONParser, parsers.MultiPartParser, ]
    pagination_class = None

    def get_permissions(self):
        if self.action == 'list':
            permission_classes = [permissions.AllowAny, ]
        else:
            permission_classes = [permissions.IsAuthenticated, ]
        return [permission() for permission in permission_classes]

    def create(self, request):

        serializer = self.serializer(data=request.data)
        serializer.context['user'] = request.user
        try:
            serializer.context['args'] = request.data.get('args')
            serializer.context['single_image'] = False
            if request.data.get('single_image'):
                serializer.context['single_image'] = True
        except KeyError:
            pass
        if serializer.is_valid():
            serializer.save()

            try:
                if serializer.context['success']:
                    serializer_prod = self.serializer_product(self.queryset_prod.filter(
                        public_id=serializer.context['success']).first())

                    return Response(serializer_prod.data, status=status.HTTP_201_CREATED)
            except KeyError:
                return Response(True, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
