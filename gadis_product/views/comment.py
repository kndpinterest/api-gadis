from database.models.comment import Comment
from django.shortcuts import get_object_or_404
from rest_framework import status, permissions
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from gadis_product.serializer.comment_serializer import CommentModelSerializer, CommentSerializer


class CommentModelViewSets(ModelViewSet):
    queryset = Comment.objects.all()
    create_serializer = CommentSerializer
    serializer_class = CommentModelSerializer

    def create(self, request):
        print(request.data)
        serializer = self.create_serializer(data=request.data)
        serializer.context['author'] = request.user.authenticate_set.first()
        if serializer.is_valid():
            serializer.save()
            serializer_com = self.serializer_class(
                Comment.objects.filter(id=serializer.context['success']).first())
            return Response(serializer_com.data, status=status.HTTP_201_CREATED)
        print(serializer.errors)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk):
        comment = get_object_or_404(Comment, pk=pk)
        comment.delete()
        return Response(True, status=status.HTTP_200_OK)
