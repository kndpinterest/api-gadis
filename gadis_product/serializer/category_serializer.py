import uuid
import logging
import os
from django.db.utils import IntegrityError
from django.conf import settings
from django.utils.translation import gettext as _
from rest_framework import serializers
from database.models.category import Product, Category
from database.models.product import ProductImage, Location, SoftImageForRemove
from gadis_product.serializer.base import Base
from gadis_authenticate.serializer.user_child_serializer import AuthorModelSerializer


class BaseCategorySerializer(Base):
    def __init__(self, instance=None, data=None, **kwargs):
        super(BaseCategorySerializer, self).__init__(
            instance=instance, data=data, **kwargs)

    @classmethod
    def create_category(cls, args, extra_fields):
        user = args.context['user']
        if not Category.objects.filter(name=extra_fields.get('name_category')).first():
            category = Category(public_id=str(uuid.uuid4()),
                                name=extra_fields.get('name_category'), author=user.authenticate_set.first())
            category.save()
        return extra_fields

    def get_fields(self, *args, **kwargs):
        fields = super(BaseCategorySerializer,
                       self).get_fields(*args, **kwargs)
        try:
            if self.context['single_image']:
                fields['single_image'].required = True

            elif self.context['args']:
                fields['name_category'].required = True
                fields['name'].required = True
                fields['description'].required = True
                fields['price'].required = True
                fields['total'].required = True
                fields['image'].required = False
                fields['city'].required = True
                fields['country'].required = True
                fields['address'].required = True
        except KeyError:
            fields['name_category'].required = True

        return fields

    @classmethod
    def create_image(cls, args, extra_fields):  # Record Single Image
        check = SoftImageForRemove.objects.filter(
            public_id=extra_fields.get('uuid')).first()
        if check:
            if check.image.count() >= 3:
                for i in check.image.all():
                    image = str(i.image.url).split('/')
                    os.system('rm assets/media/product/%s' %
                              image[len(image) - 1])
                    i.delete()
        else:
            try:
                check = SoftImageForRemove(public_id=extra_fields.get('uuid'))
                check.save()
            except IntegrityError:
                check = SoftImageForRemove.objects.filter(
                    public_id=extra_fields.get('uuid')).first()
        r_image = ProductImage(image=extra_fields.get('single_image'))
        r_image.save()
        check.image.add(r_image)
        return args

    @classmethod
    # Upload Image Many To Many
    def create_category_and_product(cls, args, extra_fields):
        user = args.context['user']
        category = Category.objects.filter(
            name=extra_fields.get('name_category')).first()
        if not category:
            category = Category(public_id=str(uuid.uuid4()),
                                name=extra_fields.get('name_category'))
            category.save()

        # Record Location
        location = Location(country=extra_fields.get('country'), city=extra_fields.get(
            'city'), address=extra_fields.get('address'), location=extra_fields.get('location'))
        location.save()

        # Record Product
        product = Product(public_id=str(uuid.uuid4()), name=extra_fields.get('name'), description=extra_fields.get(
            'description'), price=extra_fields.get('price'), total=extra_fields.get('total'),
            author=user.authenticate_set.first(), location=location)
        product.save()
        c_image = SoftImageForRemove.objects.filter(
            public_id=extra_fields.get('uuid')).first()
        if c_image:
            c_image.already_product = product
            c_image.save()

            product.image = c_image  # Add Image
            product.save()
        elif extra_fields.get('image'):

            log = logging.getLogger("Record Image Many To Many")
            log.info(True)
            c_image = SoftImageForRemove(public_id=str(
                extra_fields.get('uuid')), already_product=product)  # Record Single Image
            c_image.save()
            for i in extra_fields.get('image'):
                image = ProductImage(image=i)
                image.save()
                c_image.image.add(image)
            product.image = c_image
            product.save()
        else:
            raise serializers.ValidationError(
                {'message': _('Image cannot be blank')})
        category.product.add(product)
        args.context['success'] = product.public_id
        return args

    def create(self, validated_data):
        if self.context['args'] and validated_data.get('single_image'):
            self.create_image(self, validated_data)
            return self

        elif self.context['args']:

            log = logging.getLogger("Record Category and Product and Image")
            log.info(True)
            self.create_category_and_product(self, validated_data)
            return self
        if not validated_data.get('single_image'):
            self.create_category(self, validated_data)
        return validated_data

    def update(self, instance, validated_data):
        pass


class CategoryModelSerializer(serializers.ModelSerializer):
    # author = AuthorModelSerializer(read_only=True)

    class Meta:
        model = Category
        fields = ['name', 'icon', 'public_id']

    icon = serializers.SerializerMethodField('get_icon_display')

    def get_icon_display(self, info):
        icon = None
        direction_icon = os.listdir('assets/media/animal')
        for i in direction_icon:
            if i[0:3].lower() == info.name[0:3].lower():
                icon = i
        result = '%sanimal/%s' % (settings.MEDIA_URL, icon)
        return result
