from gadis_product.serializer.comment_serializer import CommentModelSerializer
import os
import random
from babel.numbers import format_decimal, format_currency
from django.conf import settings
from rest_framework import serializers
from database.models.product import Product, ProductImage, Location, SoftImageForRemove
from gadis_authenticate.serializer.user_child_serializer import AuthorModelSerializer
from gadis_product.serializer.support_serializer import SupportModelSerializer


class LocationModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Location
        fields = "__all__"

    soft_location = serializers.SerializerMethodField(
        'get_soft_location_display')

    def get_soft_location_display(self, info):
        result = '%s %s %s' % (info.address, info.city, info.country)
        return result


class ImageModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductImage
        fields = "__all__"


class SoftImageForRemoveSerializer(serializers.ModelSerializer):
    image = ImageModelSerializer(read_only=True, many=True)

    class Meta:
        model = SoftImageForRemove
        fields = "__all__"


class ProductModelSerializer(serializers.ModelSerializer):
    image = SoftImageForRemoveSerializer(read_only=True)
    author = AuthorModelSerializer(read_only=True)
    location = LocationModelSerializer(read_only=True)
    comment = CommentModelSerializer(read_only=True, many=True)
    support = SupportModelSerializer(read_only=True, many=True)

    class Meta:
        model = Product
        fields = "__all__"

    soft_image = serializers.SerializerMethodField('get_soft_image_display')
    totals = serializers.SerializerMethodField('get_total_display')
    custom_price = serializers.SerializerMethodField(
        'get_custom_price_display')
    soft_location = serializers.SerializerMethodField(
        'get_soft_location_display')
    supports = serializers.SerializerMethodField('get_supports_display')
    comments = serializers.SerializerMethodField('get_comments_display')

    def get_comments_display(self, info):
        return info.comment.count()

    def get_supports_display(self, info):
        return info.support.count()

    def get_soft_location_display(self, info):
        result = '%s %s %s' % (
            info.author.address, info.author.city, info.author.country)
        return result

    def get_custom_price_display(self, info):
        result = format_currency(
            info.price, 'IDR', locale='id_ID', decimal_quantization=False)

        return result

    def get_total_display(self, info):
        result = '%s / %s' % (info.sold_total, info.total)
        return result

    def get_soft_image_display(self, info):
        image = info.image
        direction = os.listdir('assets/media/avatar_soft')
        results = '%savatar_soft/%s' % (settings.MEDIA_URL,
                                        direction[random.randint(0, 1)])
        if not image:
            return results
        image = image.image.first()
        return image.image.url
