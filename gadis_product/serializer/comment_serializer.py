from gadis_authenticate.serializer.user_child_serializer import AuthorModelSerializer
from database.models.comment import Comment
from rest_framework import serializers
from gadis_product.serializer.base import Base


class CommentSerializer(Base):
    def __init__(self, instance=None, data=None, **kwargs):
        super(CommentSerializer, self).__init__(
            instance=instance, data=data, **kwargs)

    def get_fields(self, *args, **kwargs):
        fields = super(CommentSerializer, self).get_fields(*args, **kwargs)

        fields['comment'].required = True
        fields['product'].required = True
        return fields

    def create(self, validated_data):
        create = Comment(comment=validated_data.get(
            'comment'), author=self.context.get('author'))
        create.save()

        product_val = validated_data.get('product')
        product_val.comment.add(create)
        self.context['success'] = create.id
        return self


class CommentModelSerializer(serializers.ModelSerializer):
    author = AuthorModelSerializer(read_only=True)

    class Meta:
        model = Comment
        fields = "__all__"
