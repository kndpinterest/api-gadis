from database.models.comment import Comment
from database.models.product import Product
from database.models.authenticate import Authenticate
from rest_framework import serializers


class BaseLocationSerializer(serializers.Serializer):
    country = serializers.CharField(required=False)
    address = serializers.CharField(required=False)
    city = serializers.CharField(required=False)
    location = serializers.CharField(required=False)


class CategorySerializer(serializers.Serializer):
    name_category = serializers.CharField(required=False)

    class Meta:
        abstract = True


class ImageSerializer(serializers.Serializer):
    image = serializers.ListField(required=False)
    single_image = serializers.ImageField(required=False)
    uuid = serializers.CharField(required=False)

    class Meta:
        abstract = True


class ProductSerializer(serializers.Serializer):
    name = serializers.CharField(required=False)
    author = serializers.PrimaryKeyRelatedField(
        queryset=Authenticate.objects.all(), required=False)
    description = serializers.CharField(required=False)
    price = serializers.DecimalField(
        max_digits=19, decimal_places=2, required=False)
    total = serializers.IntegerField(default=0)

    class Meta:
        abstract = True


class BaseCommentSerializer(serializers.Serializer):
    comment = serializers.CharField(required=False)
    product = serializers.PrimaryKeyRelatedField(
        queryset=Product.objects.all(), required=False)


class Base(CategorySerializer, ImageSerializer, ProductSerializer, BaseLocationSerializer, BaseCommentSerializer):
    class Meta:
        abstract = True
