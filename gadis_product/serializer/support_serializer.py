from gadis_authenticate.serializer.user_child_serializer import AuthorModelSerializer
from database.models.product import SupportProduct
from rest_framework import serializers


class SupportModelSerializer(serializers.ModelSerializer):
    author = AuthorModelSerializer(read_only=True)

    class Meta:
        model = SupportProduct
        fields = "__all__"
