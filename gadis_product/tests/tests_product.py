from database.models.product import Product, SoftImageForRemove
import unittest
from database.models.authenticate import Authenticate
from rest_framework import status
from rest_framework.test import APIClient
from django.urls import reverse
import logging
from rest_framework_jwt.settings import api_settings

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER


class TestsProduct(unittest.TestCase):
    def __init__(self, methodName):
        super().__init__(methodName=methodName)
        self.authenticate = Authenticate.objects.last()
        if self.authenticate:
            self.payload = jwt_payload_handler(self.authenticate.user)
            self.encode = jwt_encode_handler(self.payload)

    def setUp(self):
        self.e = APIClient()

    @unittest.skipIf(Authenticate.objects.count() == 0, 'Authenticate not have data')
    def test_list_product(self):
        self.e.credentials(HTTP_AUTHORIZATION='Bearer ' + self.encode)
        urls = reverse('product-list')
        response = self.e.get(urls, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotEqual(response.data, None)
        log = logging.getLogger(__name__)
        log.info('Product List total %s' % len(response.data))

    @unittest.skipIf(SoftImageForRemove.objects.count() == 0, "Soft Image Not have data")
    def test_cache_product_image(self):
        urls = reverse('cache-product-image')
        response = self.e.get(urls, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotEqual(response.data, None)
        log = logging.getLogger(__name__)
        log.info('Cache Already Product')

    @unittest.skipIf(Authenticate.objects.count() == 0 or Product.objects.count() == 0, "Authenticate or Product not have data")
    def test_support_product(self):
        self.e.credentials(HTTP_AUTHORIZATION='Bearer ' + self.encode)
        product = Product.objects.all().filter(support__isnull=True).first()
        urls = reverse('support-product', args=[product.public_id])
        response = self.e.post(urls, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotEqual(response.data, None)
        log = logging.getLogger(__name__)
        log.info('Support Product')
