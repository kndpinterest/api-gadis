import unittest
import uuid
from database.models.category import Category
from rest_framework import status
from rest_framework.test import APIClient
from django.urls import reverse_lazy
import random
import logging
from database.models.authenticate import Authenticate
from rest_framework_jwt.settings import api_settings
from django.core.files import File

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER


rand = ['Bird', 'Snake', 'Fish', 'Chiken',
        'Vegetable', 'Octopus', 'Shrimp', 'Lobster', 'Bat']
name_product = ['Brocolli', 'carrot', 'cauliflower', 'corn', 'cucumber',
                'eegplant', 'green paper', 'mushrooms', 'lettuce', 'onion', 'potato', 'pumpkin']

description = [
    "Lelaki sejati itu tidak akan mengatakan cinta sebelum ada ikatan. Dia akan memendam dan memantaskan hingga nanti saatnya tiba. Lelaki itulah yg layak kau cintai. Sekian terimakasih.",
    "I'm coming from malaysia and i'm fallin in love with this song...make me feeling guilty of what i have done with my ex girlfriend...i dont even appreciate her until the day she left and never come back😢so what's the point is please appreciate your beloved cause he/she is the person who always support you in everything that you do😥please appreciate ur partner effort",
    """😢😢😢 Astaga, lagu nya buat inget dulu pas mau minang tapi ditolak keluarganya karna gw hanya karyawan swasta level bawah.. Bapaknya: bukan kami menolak, dia anak bontot dan sudah biasa manja.. Jadilah pelindung, pembimbing, pemimpin, serta pendamping yang bisa buat dia tidak pernah menangis kepada ayahnya nanti.. Berjuang dulu, karna nanti kamu tau apa maksud saya Alus bener, kyk ban yang bocor alus..
Sedikit dan tidak menyakitkan walawpun itu memukul..
Sekarang dah siap, Kamu malah udah nikah, gw masih jomblo..
Kampret..""",
    """Inget seseorang yang nyanyiin sblm jam pulang... Tp hanya sebatas kk..
Kk yang diharap lebih tp ngga mngkn...
Kagum mngkin yah... Apa lebih... ?? Yang jelas bahagia... (Romantic)"""
]

random_gio = [
    {'country': 'Rusia', 'location': '54.41967, 87.02894, 2.95',
        'states': 'Dki Jakarta', 'city': 'Jl Pemuda 61 Sentra Pemuda Kav 6, Dki Jakarta'},
    {'country': 'Uganda', 'location': '2.45733, 32.79432, 1.00',
        'states': 'Dki Jakarta', 'city': 'Jl Pemuda 61 Sentra Pemuda Kav 6, Dki Jakarta'},
    {'country': 'Idalia National Park', 'location': '-25.02312, 144.83149, 1.22',
        'states': 'Dki Jakarta', 'city': 'Jl Pemuda 61 Sentra Pemuda Kav 6, Dki Jakarta'},
    {'country': 'Finland', 'location': '62.30399, 27.09725, 4.63',
        'states': 'Dki Jakarta', 'city': 'Jl Pemuda 61 Sentra Pemuda Kav 6, Dki Jakarta'},
    {'country': 'Botswana', 'location': '-22.56263, 22.71406, 1.17',
        'states': 'Dki Jakarta', 'city': 'Jl Pemuda 61 Sentra Pemuda Kav 6, Dki Jakarta'},
]


class TestsCategory(unittest.TestCase):
    def __init__(self, methodName):
        super(TestsCategory, self).__init__(methodName=methodName)
        self.authenticate = Authenticate.objects.first()
        self.payload = jwt_payload_handler(self.authenticate.user)
        self.encode = jwt_encode_handler(self.payload)

    def setUp(self):
        self.e = APIClient()

    @unittest.skipIf(Authenticate.objects.count() == 0, 'Authenticate not have data')
    def test_record_category(self):
        self.e.credentials(HTTP_AUTHORIZATION='Bearer ' + self.encode)
        urls = reverse_lazy('api-c:categorys-list')
        data = {
            'name_category': rand[random.randint(0, 8)]
        }
        response = self.e.post(urls, data, format='multipart')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertNotEqual(response.data, None)
        log = logging.getLogger(__name__)
        log.info('Record Category')

    @unittest.skipIf(Category.objects.count() == 0, "Category not have data")
    def test_record_product(self):
        self.e.credentials(HTTP_AUTHORIZATION='Bearer ' + self.encode)
        urls = reverse_lazy('api-c:categorys-list')
        list_image = [File(open('assets/43383bbe7049f170fc60a50068af16e4.jpg', 'rb')),
                      File(open('assets/43383bbe7049f170fc60a50068af16e4.jpg', 'rb'))]
        category = Category.objects.first()
        data = {
            'name_category': category.name,
            'name': name_product[random.randint(0, 6)],
            'price': random.randint(20000, 50000),
            'description': description[random.randint(0, 2)],
            'total': random.randint(100, 200),
            'image': list_image,
            'args': 'args',
            'uuid': str(uuid.uuid4()),
            'city': random_gio[random.randint(0, 4)].get('states'),
            'address': random_gio[random.randint(0, 4)].get('city'),
            'country': random_gio[random.randint(0, 4)].get('country'),
            'location': random_gio[random.randint(0, 4)].get('location'),
        }
        response = self.e.post(urls, data, format='multipart')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertNotEqual(response.data, None)
        log = logging.getLogger(__name__)
        log.info('Record Product')

    def test_category_list(self):
        urls = reverse_lazy('api-c:categorys-list')
        response = self.e.get(urls, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotEqual(response.data, None)
        log = logging.getLogger(__name__)
        log.info('Category List total %s' % (len(response.data)))
