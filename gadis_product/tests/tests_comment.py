import unittest
import logging
import random
from django.urls import reverse
from rest_framework.test import APIClient
from rest_framework import status
from database.models.product import Product
from database.models.authenticate import Authenticate
from rest_framework_jwt.settings import api_settings

jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

comment = [
    "Thanks to the quarantine isolation I found this Drama.  and fell in love with this one.  I never saw a drama like this. The chemistry of the protagonists, the, people who surround them both, form a magnificent team.  A touching love story and at the same time fun, suspense, action, that keeps the viewer interested and moved from begining to end,  and that leaves the desire to keep watching it, over and over again. I go about my fifth time rewatching.  I wish everyone the best of luck at the 56th Baeksang Arts Awards",
    "tiktok”がダメなんじゃない。”tiktok”をやってる人達が著作権とか考えないで無断で曲を転載するからtiktokが悪く言われるんだよ。実際機能とか悪くないのに",
    """サムネにうつってる男の人くそかっけぇ

    自分のコメントにこんなにも返信頂けるとは思ってもいませんでした💦
    書かれていた名前の人全て調べました
    みなさんかっこよかったです☺️💖
    自分K‐popの事本当にわからないので
    オススメのグープとか人とか
    覚えた方がいい言葉？単語など
    沢山教えていただけると嬉しいです！""",
    """サムネ見てジェミンくんだ!!!!って思ってコメ欄見たら色々荒れてるね💦
ジェミンくんで正解です( > <)
    まずNCT DREAMのGoのコンセプトのジェミン君のイラストなので
見たらわかると思います( > <)
    似てなくても確実に髪型、表情、ファッションがgoのコンセプトのジェミンですね､､､
    今の私のプロフィール写真です!!!!""",
]


class TestsComment(unittest.TestCase):
    def setUp(self):
        self.e = APIClient()
        self.auth = Authenticate.objects.last()
        self.payload = jwt_payload_handler(self.auth.user)
        self.token = jwt_encode_handler(self.payload)

    @unittest.skipIf(Authenticate.objects.count() == 0, "Authenticate not have data")
    def test_comment(self):
        self.e.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)
        urls = reverse('api-com:comment-list')
        product = Product.objects.filter(comment__isnull=True).first()
        data = {
            'comment': comment[random.randint(0, 3)],
            'product': product.id}
        response = self.e.post(urls, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertNotEqual(response.data, None)
        log = logging.getLogger(__name__)
        log.info('Record Comment Product')
