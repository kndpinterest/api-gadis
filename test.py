from gadis_product.tests.tests_category import TestsCategory
from gadis_authenticate.tests.tests_user import TestsUser
from gadis_product.tests.tests_product import TestsProduct
from gadis_graphql.tests.tests_schema import TestsSchema
from gadis_product.tests.tests_comment import TestsComment
from gadis_message.tests.tests_room import TestsRoom
import unittest
import coloredlogs
import logging
import sys

coloredlogs.install()


logging.basicConfig(stream=sys.stdout, level=logging.DEBUG,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

if __name__ == "__main__":
    unittest.main()
