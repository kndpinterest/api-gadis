from django.urls import path, include

urlpatterns = [
    path('user/', include('gadis_authenticate.urls.user')),
    path('product/', include('gadis_product.urls.category')),
    path('message/', include('gadis_message.urls.room'))
]
